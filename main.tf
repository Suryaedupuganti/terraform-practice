provider "aws" {
  region  = "ap-south-1"
  version = "~> 2.41"
}

terraform {
  backend "s3" {
    region  = "ap-south-1"
    encrypt = true
    bucket  = "surya-terraform-practice-ap-south-1"
    key     = "dev/win-tf-worker-nodes.tfstate"
  }
}

